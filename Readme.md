# COPYRIGHT

  - Copyright (c) 2003 by Frank J. Perricone, all rights reserved
  - hawthorn@foobox.com


# PURPOSE

Hârn Astrolabe puts at your fingers a bunch of useful bits of information from the calendar of Hârn.  For any day you can see the season, sunrise, sunset, moonrise, moonset, moon phase, religious calendar, and a day's worth of weather (generated according to the chart in the Hârn regional module, with some slight modifications noted below).

Note: some of the information displayed here is based on assumptions that are not explicitly provided for in Hârn canon.  For instance, there is insufficient information given in the Hârn Regional Module to make precise calculations about sun and moon rise and set times.  Other Hârn scholars have pieced together assumptions necessary to make these calculations, particularly on the HârnList, and their answers have become fairly widely accepted.  (Though in email conversations with them, these sages always insist that their assumptions are arbitrary, and that there may be no way to make assumptions consistent with both the Hârn canon and astrophysics as we know it!  But the imprecisions are too small to be of great concern to the vast majority of us.)  As I've used their results, the output of Hârn Astrolabe is not official or canon!  But dare your players to prove it.  :)


# DISTRIBUTION AND COPYRIGHT

Hârn Astrolabe is FREEWARE.  Feel free to distribute it to anyone you like in any way you like, and use it freely for any purpose.  If you want to pay for it anyway, send your payment to the Nature Conservancy or the American Diabetes Association.

Though it's freeware, it's not public domain.  I retain my copyrights.  But I only do so in order to maintain some credit for my work.  If you'd like to get a copy of the source code, just ask (it's written in NSBasic/Palm v3.0.2).  I don't even mind if you make modifications or use bits of my source code in your own projects -- all I ask is that if you use some of my work you give me some credit for it, and that if you release a modified version of my program you label it as such, and that you let me know what you've done.

If you do make improvements, keep me informed.  Instead of having us release two parallel versions which confuses users, let's integrate our efforts.


# CREDITS

Many parts of this program are built firmly on the foundations of the work of others.  I didn't really create anything here, except perhaps a solution to the "weather consistency problem" (described below).  All I really did was gather information from several other people and put it into one program on the Palm.  Particular thanks are due to:

 * Columbia Games, of course, for creating Hârn and the weather rules and other details
 * Robert Schmunk <rbs@panix.com> for sunrise and sunset calculations
 * Rick Ivansek <agrafix@IX.NETCOM.COM> for moonrise and moonset calculations
 * Ketherian <ketherian@ork.net> for the religious calendar on his Pax Tharda web site, with Peonian saint's days added
 * Gary Ashburn <mask@YODA.FDT.NET> for weather generation algorithms (from his excellent WG_CT21.EXE program)
 
Thanks are also due to my alpha and beta testers, particularly the members of my realspace RPG group, including Siobhan Perricone, Chuck Lloyd, and Lane Safford.


# INSTALLATION

Simply install all three of the other files in this archive.  The files are:

 * Harn Astrolabe.prc   19K  the actual program
 * AstrolabeLookup.pdb  36K  the charts and data for the program
 * NSBRuntime.prc       98K  latest version; install even if you have an older one

Note: if you have an older version of NSBRuntime and you don't install the current one, Hârn Astrolabe will crash your Palm.  Sorry, there's nothing I can do about that.  The newer version of NSBRuntime will work with your older apps, and as of this writing it's the most current one available; so just install the included one whether you have one or not.  Play safe!

Note: if you want to beam this program, you will need to be able to also beam the NSBRuntime (you can do that in Applications).  More difficultly, you need to beam the AstrolabeLookup database.  Hârn Astrolabe doesn't currently provide a method for doing this.  If you have a general-purpose program like RsrcEdit, that can be used to beam any database (though you might have to beam RsrcEdit to the receiving Palm first to make it work!).  Without something like this, you can't currently beam the program in a usable state.  (Sorry!  Maybe in a future version, if beaming support is added to NSBasic by then.)


# USING HÂRN ASTROLABE

Just use the pulldown menus for month, day, and year to choose a date, and the information for that date fills the screen.  There are back and forward arrow buttons you can use to scroll through the days one by one.

If there are masses on the chosen date, all you'll see is the names of the gods to whom those masses belong.  Tap on the masses to pop up a window showing more detail.  These masses come Ketherian's religious calendar from Pax Tharda, and include some Peonian saint's days that aren't in the basic calendar found in HârnPlayer and elsewhere.

When reading the weather, note that several abbreviations are used to make it all fit.  Only "subjective" descriptions are given, suitable for reading out loud -- characters on Hârn are unlikely to have thermometers or wind speed gauges.  You can tap on any weather report to get more detail about it, including an expansion of the abbreviations.  For instance, "lt rain" refers either to intermittent rain showers or to a light but constant drizzle; the main screen only says "lt rain" but the weather details screen says "Rain showers or light rain".

Units used in the weather details depend on the choices you make in Settings.  (Get to Settings using the menu.)  Don't worry about the "World" popup you see there yet, it'll be discussed later.


# WEATHER CONSISTENCY

There's one challenge in developing something like this for a Palm that you don't have to worry about when doing a PC program that generates a whole bunch of weather at one sitting.  I call it "the weather consistency problem".

If you run another weather generator twice and get two different sets of results, that's not a problem.  You'll probably only run it once and save the results, referring to them from then on.  But consider this scenario.  Suppose you go into Hârn Astrolabe, get weather for today, and play out half the day.  The weather started warm and gets colder.  Then you leave Hârn Astrolabe and go into another program for a bit, or even go check another day's weather and come back.  What if the weather you see is different from what you saw before?  What if the new weather forecast has it getting hot as the day progresses instead of cold, as you've been describing so far?

Clearly, you don't want to rerandomize the weather every time you leave the program and come back into it.  But you certainly don't want to have to *write down* the weather the first time you go to the program!  Nor do you want your Palm to clutter up its limited memory with recordings of the weather of hundreds or thousands of shifts you've previously glanced at!

To get around these problems I've used a sneaky little trick.  All the die rolls used to generate weather are not made randomly; they're derived from some mathematical formulae based on the date (and one other factor) so they're completely repeatable.  They're only "psuedo-random" -- they look random, they're complex enough to be hard to predict, but the computer can get the same results every time.  Thus, every time you look at the weather for Nuzyael 22, 720, you get the same results.  And all without the Palm having to save anything at all in its precious memory.

This isn't as strange as it sounds, since true randomness isn't really possible on computers anyway.  They always simulate it through psuedo-randomness, using calculations very similar to the ones I've used, but they take one element of randomness they can get from the user -- such as the exact time, to the second, that the user runs the program in the first place -- and use it to "seed" the rest of the calculations with "true" randomness.  If you use the same seed and the same algorithm you always get the same numbers, but the odds of running the program at the same instant of the day as last time are so low that it's negligible for most purposes.

All I've done is make the "seed" depend on the date, and one more factor that the user controls, so the results are *intentionally* repeatable.  Unfortunately since NSBasic gives me no way to use its internal randomization algorithms without letting it pick the seed for me, I had to do my own, and they're probably not as robust, but they'll do.

The goal here is to make sure that if the GM goes to the weather for Nuzyael 22, 720, she gets the same results every time.  A positive side effect is, if players who also have Palms load Hârn Astrolabe and go to Nuzyael 22, 720, they'll also see the right weather.  (I assume if you have players who would "cheat" by looking at future weather, you already have plans to either "cheat" back at them, or just thwap them upside the head with a ruler.)

A negative side effect is, if you play a few campaigns, players will get used to seeing the same weather game after game on the same days.  Both to solve this, and to add more psuedo-randomness, I've added one more user choice (located on the Settings screen): you must pick a world from A to J, using the pulldown in the upper right corner.  If everyone picks the same world they all get the same weather, but you can choose different worlds for different campaigns.  Once you've picked a world for a campaign, stick with it.

Note that there's no pattern to what kind of weather each world gets -- it's not like World C gets colder weather and World G gets windy weather.  They all get the same kinds of weather, since they all work from the same rules.  They just get different specifics on different days.  Think of each "world" like a different set of pre-determined dice rolls used on the same charts to generate weather.

There's not quite enough psuedo-randomness in this approach to completely account for all possible combinations of rolls.  In a world whose weather is determined by this program, an extremely fastidious scholar going over decades of detailed weather records and carefully analyzing them for patterns might, with luck, find some combinations which never occur, or other patterns, which cannot be explained.  But it's doubtful players could ever notice.  (Though don't you sometimes wonder if the world we live in isn't being simulated on a Commodore 64?)


# THE MIDNIGHT PROBLEM

There's one flaw in this approach which is either glaring and awful, or not a big deal, depending on your viewpoint.

Normally, the Hârn weather rules start you off with a d20 roll at the beginning of the campaign, and from then on, one shift's weather depends on the previous shift's weather, forever and ever.  You never roll a d20 again to "reset" things.  If you're writing a program that generates a month's or year's worth of weather at one sitting, that's no problem, since you'll be doing it one shift at a time, one day at a time, in sequence.  If you sit down today and generate Nuzyael's weather, it'll all be continuous.

When it comes time to generate Peonu's weather, there's a tiny problem.  Unless the weather generation program lets you record the ending "counter position" from the last shift of Nuzyael, and feed it into the program when you start generating Peonu's weather (and I am not aware of any existing program that lets you do that), the program will roll a new d20 and "reset" the weather on the first shift of Peonu.  This creates a "disjunction" -- a moment when, at midnight, the weather might change abruptly, the weather in one shift having no relation to the weather in the previous shift.

Is this a problem?  Probably not; if you generate a month or year of weather at once, the disjunctions will be rare enough that players won't notice, let alone care.  But Hârn Astrolabe only generates one day's weather at a time.  Thus, Hârn Astrolabe is forced to have the same disjunction *every day* at midnight.  The rest of the day, the weather in one shift is usually very similar to the weather in the previous shift, but at midnight, the sky resets, rerolls the dice, and starts over.  I call this, unpoetically, "the midnight problem".

Solutions to this problem have so far eluded me.  When you get right down to it, if you change the month pulldown from Nuzyael to Morgat, the only way to figure out the weather on that day in Morgat is to figure out the weather of every single shift of every single day in between!  Clearly this is impractical on the Palm, and leads back to the old problem of pre-generating and storing the weather for every shift in the entire 20-year range of Hârn Astrolabe.

This problem probably won't interfere much with actual play.  The GM can easily "blur" this disjunction, particularly if, like most people in a medieval world, the player characters spend most of their midnights either asleep or indoors.  It means the weather might feel a bit more "changeable" than it would otherwise feel by the charts, in your incarnation of Hârn.  "If you don't like the weather, wait a day, it'll change", your characters can comment with wry, self-amused seriousness.  (Now that I think about it, I think I've lived in a few places where that Commodore 64's software has the same problem!)

Nevertheless, even if it never bugs anyone in actual play, this problem bugs me, and I'm striving to come up with a clever solution that doesn't break the clever solution to the weather consistency problem, or strain the abilities of a Palm.  Maybe in a future version.  (If you have an idea, share it with me!)


# WIND VARIABILITY

The psuedo-randomness approach mentioned gives me a limited amount of psuedo-randomness to play with.  One area where I decided to cut corners to make ends meet is wind variability.  Per the rules, each shift has a variability in weather of 0-2 points on the 0-4 scale, though they suggest that rather than randomizing, you logically adapt the wind to the circumstances.  I've simply chosen a single d3 roll for the entire day.  That doesn't mean the wind never changes within a day; it just means the only changes are those indicated by changes in the counter position.  Frankly, I think the rules provide too much variability, and my program provides too little, but my program is a bit closer to ideal.  And again, it's close enough for most games.


# FUTURE IMPROVEMENTS

The following improvements are planned (or at least hoped) for future versions.

1. Faster display of the weather.  The slowdown is due to NSBasic's dodginess concerning building strings.  Speedup tricks are possible (you'd be amazed how much slower it was before I used those tricks everywhere else) but will require a major revamp to use for weather.
2. User-defined calendar events.  This will work like a Note does in your basic Palm apps; if an event (like a PC's birthday) is defined for a particular date, a paper icon will appear.  Tap it, or use a menu command Attach Note, to open up a Note form for that date's note.  Notes will be associated with dates of the year (e.g., 21 Savor), not specific days (e.g., 21 Savor 693).
3. Maybe someday I'll find a solution for the midnight problem!
4. (Optional) improvements to the weather generation rules from those provided in the Hârn regional module, some derived from Gary Ashburn's excellent WG_CT21.EXE (available for download on the HârnPage).  The basic rules are a good balance between how much work you have to do and getting realistic results, but we have a computer here which can do more work cheerfully, so maybe we can use it to get better results.  Here are some issues I hope to address:

*  In real life, temperatures on most days tend to be highest in mid-afternoon and lowest in the wee hours, but in this system, it's as likely to be hot at 8am and cool at 4pm as the other way around.
*  It's exactly as likely to snow on the last day of spring as on the first.  Or to be hot, or thunderstormy, etc.
*  Wind changes tend to be pretty abrupt.
*  It'd be nice to have actual temperatures and windspeeds sometimes, even if the characters can't measure them.
*  It might also be nice sometimes to have more detail about other weather elements.  For instance, how much snow fell, and was it a wet, sloppy snow, or a dry, powdery snow?  The GM can always make this stuff up, but sometimes it's nice to offload that burden onto an assistant, and save your creativity for other things.
*  There are a few kinds of weather not modeled at all in this system.
*  In essence, there's a mini-disjunction every four hours which the GM easily blurs over.  Maybe it'd be nice if the program helped with that blurring.
*  The system doesn't account for being in the mountains, in a valley, by the beach, etc.; nor whether you're on the northern or southern end of Hârn.  (For that matter, sunrise, sunset, moonrise, and moonset don't account for that either, and probably should.)


# KNOWN BUGS

There's a known quirk that affects Palm Vs running OS3.5 (I'm not sure if it's linked to the ROM or the OS, so it might affect other models) which affects NSBasic programs.  Something in the way the Palm OS handles event triggers is a bit screwy.  As a result, if you tap on a pulldown menu, then tap on one of the options, Palm OS changes the menu choice like it should, and then *also* tells the program that you tapped on the spot on the screen that was under the menu.  In this case, if the menu item you choose is right over the masses or weather, that'll bring up the "extra details" windows for the masses or weather, just as if you'd tapped on them.

Other than hoping someone figures out a fix or workaround for this in the future, there's nothing I can do about this, short of a complete screen redesign to make sure no pulldown menu is ever "over" anything tappable -- and that would make it impossible to show the information that's on the screen right now.  As this should only affect a small number of users, I'm going to leave it like this.


# VERSION HISTORY

 * v0.81 03-04-2003 FJP: First alpha-test release.
 * v0.82 03-05-2003 FJP: Made the weather times tappable as well as the weather reports.
 * v0.83 03-07-2003 FJP: A few tweaks brought about while attempting to work around the pulldown menu bug.  Several speedups.
 * v0.90 03-07-2003 FJP: First beta-test release.
 * v1.00 03-15-2003 FJP: Added Settings form and moved World selection to it.  Added support for alternate units in weather details.  More speedups.